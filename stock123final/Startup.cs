﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(stock123final.Startup))]
namespace stock123final
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
