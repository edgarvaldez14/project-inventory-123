﻿

namespace stock123final.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DbModelsEntities : DbContext
    {
        public DbModelsEntities()
            : base("name=DbModelsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<almacen> almacens { get; set; }
        public virtual DbSet<articulo> articuloes { get; set; }
        public virtual DbSet<tipo_inventario> tipo_inventario { get; set; }
        public virtual DbSet<transaccione> transacciones { get; set; }
        public virtual DbSet<asiento_contable> asiento_contable { get; set; }
    }
}
