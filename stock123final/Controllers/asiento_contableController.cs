﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using stock123final.Models;
using System.IO;

namespace stock123final.Controllers
{
    public class asiento_contableController : Controller
    {
        private DbModelsEntities db = new DbModelsEntities();

        // GET: asiento_contable
        public ActionResult Index()
        {
            var asiento_contable = db.asiento_contable.Include(a => a.tipo_inventario1);
            return View(asiento_contable.ToList());
        }

        

        // GET: asiento_contable/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            asiento_contable asiento_contable = db.asiento_contable.Find(id);
            if (asiento_contable == null)
            {
                return HttpNotFound();
            }
            return View(asiento_contable);
        }

        // GET: asiento_contable/Create
        public ActionResult Create()
        {

            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id_tipo", "descripcion");
            return View();
        }

        // POST: asiento_contable/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_auxiliar,descripcion,tipo_inventario,cuenta_contable,tipo_movimiento,fecha_asiento,monto_asiento,estado_asiento")] asiento_contable asiento_contable)
        {
            
            
            if (ModelState.IsValid)
            {
                db.asiento_contable.Add(asiento_contable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id_tipo", "descripcion", asiento_contable.tipo_inventario);
            return View(asiento_contable);
        }


        
        
           public void Export([Bind(Include = "id,id_auxiliar,descripcion,tipo_inventario,cuenta_contable,tipo_movimiento,fecha_asiento,monto_asiento,estado_asiento")]asiento_contable asiento_contable, int id)
           {
           SOAPService.WSXContabilidadSoapClient WS = new SOAPService.WSXContabilidadSoapClient();
            
                using (DbModelsEntities temp = new DbModelsEntities())
                    {

                    List<asiento_contable> asList = temp.asiento_contable.Where(x=>x.id == id).ToList();
                    if (asList.Count > 0)
                    {
                        var xEle = new XElement("Asientos",
                        from emp in asList
                        select new XElement("Asientos",
                        new XElement("Descripcion", emp.descripcion),
                        new XElement("Identificador", emp.id_auxiliar),
                        new XElement("Cuenta", emp.cuenta_contable),
                        new XElement("TipoMovi", emp.tipo_movimiento),
                        new XElement("Monto", emp.monto_asiento)
                        ));

                        this.HttpContext.Response.Write(xEle);
                        this.HttpContext.Response.ContentType = "application/xml";
                        this.HttpContext.Response.AppendHeader("Content-Disposition", "attachment; filename=AsientoData.xml");
                        this.HttpContext.Response.End();

                    }

                }      

           }

        [HttpPost]
        public ActionResult Exportar(HttpPostedFileBase xmlFile)
        {

            {
                
                if (!Path.GetFileName(xmlFile.FileName).EndsWith(".xml"))
                {
                    ViewBag.Error = string.Format("El tipo del Archivo no es valido");
                }
                else
                {
                    SOAPService.WSXContabilidadSoapClient WS = new SOAPService.WSXContabilidadSoapClient();
                    xmlFile.SaveAs(Server.MapPath("~/XmlFiles" +
                         Path.GetFileName(xmlFile.FileName)));
                    ViewBag.Message = string.Format("Archivo enviado con exito");

                }
                
            }
            return RedirectToAction("Index");
        }

        public ActionResult Enviar([Bind(Include = "id,id_auxiliar,descripcion,tipo_inventario,cuenta_contable,tipo_movimiento,fecha_asiento,monto_asiento,estado_asiento")]asiento_contable asiento_contable, int id)
        {

            using (DbModelsEntities temp = new DbModelsEntities())
            {



                List<asiento_contable> asList = temp.asiento_contable.Where(x => x.id == id).ToList();
                if (asList.Count > 0)
                {
                    try
                    {
                        asiento_contable = db.asiento_contable.Find(id);
                        SOAPService.WSXContabilidadSoapClient wsClient = new SOAPService.WSXContabilidadSoapClient();
                        string idAsiento = wsClient.registrarAsiento(asiento_contable.descripcion, 4, Convert.ToDouble(asiento_contable.monto_asiento), 6, 82).ToString();
                    }
                    catch(Exception e)
                    {

                    }

                }
            }
            return RedirectToAction("Index");
           
        }
        // GET: asiento_contable/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            asiento_contable asiento_contable = db.asiento_contable.Find(id);
            if (asiento_contable == null)
            {
                return HttpNotFound();
            }
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id_tipo", "descripcion", asiento_contable.tipo_inventario);
            return View(asiento_contable);
        }

        // POST: asiento_contable/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_auxiliar,descripcion,tipo_inventario,cuenta_contable,tipo_movimiento,fecha_asiento,monto_asiento,estado_asiento")] asiento_contable asiento_contable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(asiento_contable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id_tipo", "descripcion", asiento_contable.tipo_inventario);
            return View(asiento_contable);
        }

        // GET: asiento_contable/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            asiento_contable asiento_contable = db.asiento_contable.Find(id);
            if (asiento_contable == null)
            {
                return HttpNotFound();
            }
            return View(asiento_contable);
        }

        // POST: asiento_contable/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            asiento_contable asiento_contable = db.asiento_contable.Find(id);
            db.asiento_contable.Remove(asiento_contable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
