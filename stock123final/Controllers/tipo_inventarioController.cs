﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using stock123final.Models;

namespace stock123final.Controllers
{
    public class tipo_inventarioController : Controller
    {
        private DbModelsEntities db = new DbModelsEntities();

        // GET: tipo_inventario
        public ActionResult Index()
        {
            var tipo_inventario = db.tipo_inventario.Include(t => t.almacen);
            return View(tipo_inventario.ToList());
        }

        // GET: tipo_inventario/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipo_inventario tipo_inventario = db.tipo_inventario.Find(id);
            if (tipo_inventario == null)
            {
                return HttpNotFound();
            }
            return View(tipo_inventario);
        }

        // GET: tipo_inventario/Create
        public ActionResult Create()
        {
            ViewBag.id_almacen = new SelectList(db.almacens.Where(t => t.estado_almacen == true).ToList(), "id_almacen", "descripcion");
            ViewBag.Error = TempData["Error"];
            return View();
        }

        // POST: tipo_inventario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_tipo,id_almacen,descripcion,cuenta_contable,estado_tipo")] tipo_inventario tipo_inventario)
        {
            if (ModelState.IsValid)
            {
                db.tipo_inventario.Add(tipo_inventario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_almacen = new SelectList(db.almacens, "id_almacen", "descripcion", tipo_inventario.id_almacen);
            return View(tipo_inventario);
        }

        // GET: tipo_inventario/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipo_inventario tipo_inventario = db.tipo_inventario.Find(id);
            if (tipo_inventario == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_almacen = new SelectList(db.almacens.Where(t => t.estado_almacen == true).ToList(), "id_almacen", "descripcion", tipo_inventario.id_almacen);
            ViewBag.Error = TempData["Error"];
            return View(tipo_inventario);
        }

        // POST: tipo_inventario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_tipo,id_almacen,descripcion,cuenta_contable,estado_tipo")] tipo_inventario tipo_inventario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipo_inventario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_almacen = new SelectList(db.almacens, "id_almacen", "descripcion", tipo_inventario.id_almacen);
            return View(tipo_inventario);
        }

        // GET: tipo_inventario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipo_inventario tipo_inventario = db.tipo_inventario.Find(id);
            if (tipo_inventario == null)
            {
                return HttpNotFound();
            }
            return View(tipo_inventario);
        }

        // POST: tipo_inventario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tipo_inventario tipo_inventario = db.tipo_inventario.Find(id);
            db.tipo_inventario.Remove(tipo_inventario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
