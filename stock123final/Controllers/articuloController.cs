﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using stock123final.Models;

namespace stock123F.Controllers
{
    public class articuloController : Controller
    {
        private DbModelsEntities db = new DbModelsEntities();

        // GET: articulo
        public ActionResult Index()
        {
            var articuloes = db.articuloes.Include(a => a.tipo_inventario1);
            return View(articuloes.ToList());
        }

        // GET: articulo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            articulo articulo = db.articuloes.Find(id);
            if (articulo == null)
            {
                return HttpNotFound();
            }
            return View(articulo);
        }

        // GET: articulo/Create
        public ActionResult Create()
        {
            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario.Where(t => t.estado_tipo == true).ToList(), "id_tipo", "descripcion");
            ViewBag.Error = TempData["Error"];
            return View();
        }

        // POST: articulo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_articulo,descripcion,existencia,tipo_inventario,costo_unitario,estado_articulo")] articulo articulo)
        {
            if (ModelState.IsValid)
            {
                db.articuloes.Add(articulo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario, "id_tipo", "descripcion", articulo.tipo_inventario);
            return View(articulo);
        }

        // GET: articulo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            articulo articulo = db.articuloes.Find(id);
            if (articulo == null)
            {
                return HttpNotFound();
            }

            ViewBag.tipo_inventario = new SelectList(db.tipo_inventario.Where(t => t.estado_tipo == true).ToList(), "id_almacen", "descripcion", articulo.tipo_inventario);
            ViewBag.Error = TempData["Error"];
            return View(articulo);
        }

        // POST: articulo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_articulo,descripcion,existencia,tipo_inventario,costo_unitario,estado_articulo")] articulo articulo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(articulo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_tipo = new SelectList(db.tipo_inventario, "id_tipo", "descripcion", articulo.tipo_inventario);
            return View(articulo);
        }

        // GET: articulo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            articulo articulo = db.articuloes.Find(id);
            if (articulo == null)
            {
                return HttpNotFound();
            }
            return View(articulo);
        }

        // POST: articulo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            articulo articulo = db.articuloes.Find(id);
            db.articuloes.Remove(articulo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
